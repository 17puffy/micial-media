<?php

namespace App\Repository;

use App\Authentication\Authentication;
use App\Database\ConnectionHandler;
use Exception;

/**
 * Das UserRepository ist zuständig für alle Zugriffe auf die Tabelle "user".
 *
 * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
 */
class UserRepository extends Repository {
     /**
      * Erstellt einen neuen benutzer mit den gegebenen Werten.
      *
      * Das Passwort wird vor dem ausführen des Queries noch mit dem SHA1
      *  Algorythmus gehashed.
      *
      * @param $name Wert für die Spalte name
      * @param $lastname Wert für die Spalte lastname
      * @param $username Wert für die Spalte username
      * @param $email Wert für die Spalte email
      * @param $password Wert für die Spalte password
      *
      * @throws Exception falls das Ausführen des Statements fehlschlägt
      */

     public function create($profilePicture, $name, $lastname, $username, $email, $password) {
          $password = password_hash($password, PASSWORD_DEFAULT);

          $query = "INSERT INTO {$this->tableName} (name, lastname, username, email, profilepicture, password) VALUES (?, ?, ?, ?, ?, ?)";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ssssss', $name, $lastname, $username, $email, $profilePicture, $password);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }

     public function getUserByUname($username) {
          $query = "SELECT * from user WHERE username = ?;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('s', $username);
          $statement->execute();
          $result = $statement->get_result();

          if (!$result) {
               throw new Exception($statement->error);
          }
          return $result;
     }

     /**s
      * Diese Variable wird von der Klasse Repository verwendet, um generische
      * Funktionen zur Verfügung zu stellen.
      */
     protected $tableName = 'user';

     public function checkPassword($username, $password) {
          $query = "SELECT * from {$this->tableName} WHERE username = ?;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('s', $username);
          $statement->execute();
          $result = $statement->get_result();

          if (!$result) {
               throw new Exception($statement->error);
          }

          // Falls nur ein user mit den eingegebenen Daten existiert

          if ($result->num_rows == 1) {
               $row = $result->fetch_object();
               $result->close();
               $passwordHashFromDB = $row->password;

               if (password_verify($password, $passwordHashFromDB)) {
                    return true;
               } else {
                    return false;
               }
          } else {
               return false;
          }
     }

     public function editProfile($newUsername, $newFirstname, $newLastname, $profilePicture, $username) {
          $query = "UPDATE {$this->tableName} SET username = ?, name = ?, lastname = ?, profilepicture = ? WHERE username = ?;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('sssss', $newUsername, $newFirstname, $newLastname, $profilePicture, $username);
          $statement->execute();
     }

     public function changePW($password, $username) {
          $password = password_hash($password, PASSWORD_DEFAULT);
          $query = "UPDATE {$this->tableName} SET password = ? WHERE username = ?;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ss', $password, $username);
          $statement->execute();
     }

     public function getProfilePicture($username) {
          $query = "SELECT profilepicture from {$this->tableName} WHERE username = ?";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('s', $username);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }
          $result = $statement->get_result();

          if ($result->num_rows == 1) {
               $row = $result->fetch_object();
               $result->close();
               return $row->profilepicture;
          }
     }
}