<?php

namespace App\Repository;

use App\Database\ConnectionHandler;
use Exception;

/**
 * Das PostRepository ist zuständig für alle Zugriffe auf die Tabelle "post".
 *
 * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
 */
class LikeRepository extends Repository {
     /**
      * Diese Variable wird von der Klasse Repository verwendet, um generische
      * Funktionen zur Verfügung zu stellen.
      */
     protected $tableName = 'like';

     /**
      * Erstellt einen neuen post mit den gegebenen Werten.
      *
      * @param $userID Wert für die Spalte user_id
      * @param $postID Wert für die Spalte post_id
      *
      * @throws Exception falls das Ausführen des Statements fehlschlägt
      */
     public function create($userID, $postID) {
          $query = "INSERT INTO `{$this->tableName}` (user_id, post_id) VALUES (?, ?)";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ii', $userID, $postID);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }

     public function readByIds($userID, $postID) {
          // Query erstellen
          $query = "SELECT * FROM `{$this->tableName}` WHERE user_id = ? AND post_id = ?";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ii', $userID, $postID);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Den gefundenen Datensatz zurückgeben
          return $result->num_rows;
     }

     public function deleteByIds($userID, $postID) {
          $query = "DELETE FROM `{$this->tableName}` WHERE user_id = ? AND post_id = ?;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ii', $userID, $postID);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }
     }

     public function readByUserId($userID) {
          // Query erstellen
          $query = "SELECT * FROM `{$this->tableName}` WHERE user_id = ?";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $userID);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Ersten Datensatz aus dem Reultat holen
          $rows = array();
          while ($row = $result->fetch_object()) {
               $rows[] = $row->post_id;
          }

          // Datenbankressourcen wieder freigeben
          $result->close();

          // Die gefundenen Datensätze zurückgeben
          return $rows;
     }

     public function countByPostId($postID) {
          // Query erstellen
          $query = "SELECT count(post_id) as likes FROM `{$this->tableName}` WHERE post_id = ?";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $postID);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Ersten Datensatz aus dem Reultat holen
          $row = $result->fetch_object();

          // Datenbankressourcen wieder freigeben
          $result->close();

          // Die gefundenen Datensätze zurückgeben
          return $row->likes;
     }
}
