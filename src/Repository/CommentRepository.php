<?php


namespace App\Repository;

use App\Database\ConnectionHandler;
use Exception;


class CommentRepository extends Repository {
     /**
      * Das PostRepository ist zuständig für alle Zugriffe auf die Tabelle "post".
      *
      * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
      */

     /**
      * Diese Variable wird von der Klasse Repository verwendet, um generische
      * Funktionen zur Verfügung zu stellen.
      */
     protected $tableName = 'comment';

     /**
      * Erstellt einen neuen post mit den gegebenen Werten.
      *
      * @param $text Wert für die Spalte text
      * @param $userID Wert für die Spalte user_id
      * @param $postID Wert für die Spalte post_id
      * @param $date Wert für die Spalte date
      * @param $username Wert für die Spalte username
      *
      * @throws Exception falls das Ausführen des Statements fehlschlägt
      */
     public function create($text, $userID, $postID, $username, $date) {
          $query = "INSERT INTO $this->tableName (text, user_id, post_id, username, date) VALUES (?, ?, ?, ?, ?)";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('siiss', $text, $userID, $postID, $username, $date);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }

     public function readById($id) {
          // Query erstellen
          $query = "SELECT * FROM {$this->tableName} WHERE id = ?";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $id);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }
          $row = $result->fetch_object();
          $result->close();

          // Den gefundenen Datensatz zurückgeben
          return $row;
     }

     public function readByPostId($id) {
          // Query erstellen
          $query = "SELECT comment.*, user.profilepicture, user.username FROM {$this->tableName} join user on {$this->tableName}.user_id = user.id  WHERE post_id = ? order by date";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $id);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Den gefundenen Datensatz zurückgeben
          return $result;
     }

     public function update($commentID, $text) {
          $query = "UPDATE $this->tableName SET text = ? WHERE id = ?";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('si', $text, $commentID);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }
}
