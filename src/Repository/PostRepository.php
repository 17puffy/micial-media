<?php

namespace App\Repository;

use App\Database\ConnectionHandler;
use Exception;

/**
 * Das PostRepository ist zuständig für alle Zugriffe auf die Tabelle "post".
 *
 * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
 */
class PostRepository extends Repository {
     /**
      * Diese Variable wird von der Klasse Repository verwendet, um generische
      * Funktionen zur Verfügung zu stellen.
      */
     protected $tableName = 'post';

     /**
      * Erstellt einen neuen post mit den gegebenen Werten.
      *
      * @param $title Wert für die Spalte title
      * @param $text Wert für die Spalte text
      * @param $userID Wert für die Spalte user_id
      * @param $date Wert für die Spalte date
      *
      * @throws Exception falls das Ausführen des Statements fehlschlägt
      */
     public function create($title, $text, $userID, $date) {
          $query = "INSERT INTO $this->tableName (title, text, user_id, date) VALUES (?, ?, ?, ?)";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ssis', $title, $text, $userID, $date);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }

     public function update($title, $text, $postID) {
          $query = "UPDATE $this->tableName SET title = ?, text = ? WHERE id = ?";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('ssi', $title, $text, $postID);

          if (!$statement->execute()) {
               throw new Exception($statement->error);
          }

          $statement->get_result();
          return $statement->insert_id;
     }

     public function readByUserId($userID) {
          // Query erstellen
          $query = "SELECT p.*, u.profilepicture, u.username, u.verification FROM {$this->tableName} as p join user as u on p.user_id = u.id WHERE user_id=? ORDER BY p.date desc";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $userID);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          $rows = array();
          while ($row = $result->fetch_object()) {
               $rows[] = $row;
          }
          // Den gefundenen Datensatz zurückgeben
          return $rows;
     }

     public function readAllPosts($userID) {
          $query = "Select p.*, (l.user_id is not null) as `like`, u.username, u.profilepicture, u.verification from $this->tableName as p left join (select * from `like` where user_id = ?)  as l on l.post_id = p.id join user as u on u.id = p.user_id ORDER BY p.date desc;";

          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $userID);
          $statement->execute();

          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Datensätze aus dem Resultat holen und in das Array $rows speichern
          $rows = array();
          while ($row = $result->fetch_object()) {
               $rows[] = $row;
          }

          return $rows;
     }

     public function readByPostId($id) {
          // Query erstellen
          $query = "SELECT * FROM {$this->tableName} WHERE id=?";

          // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
          // und die Parameter "binden"
          $statement = ConnectionHandler::getConnection()->prepare($query);
          $statement->bind_param('i', $id);

          // Das Statement absetzen
          $statement->execute();

          // Resultat der Abfrage holen
          $result = $statement->get_result();
          if (!$result) {
               throw new Exception($statement->error);
          }

          // Ersten Datensatz aus dem Reultat holen
          $row = $result->fetch_object();

          // Datenbankressourcen wieder freigeben
          $result->close();

          // Den gefundenen Datensatz zurückgeben
          return $row;
     }
}
