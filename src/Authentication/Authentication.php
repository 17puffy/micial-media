<?php

namespace App\Authentication;

use App\Repository\UserRepository;
use RuntimeException;

class Authentication {
     public static function login($username, $password) {
          unset($errorForUser);
          // Den Benutzer anhand der E-Mail oder des Benutzernamen auslesen

          $userRepository = new UserRepository();
          $result = $userRepository->getUserByUname($username);

          // Falls nur ein user mit den eingegebenen Daten existiert
          if ($result->num_rows == 1) {
               $user = $result->fetch_object();
               $result->close();
               // Prüfen ob der Password-Hash dem aus der Datenbank entspricht
               if (password_verify($password, $user->password)) {
                    // Login successful
                    $_SESSION['userID'] = $user->id;
                    $_SESSION['loggedin'] = true;
                    $_SESSION['username'] = $user->username;
                    $_SESSION['name'] = $user->name;
                    $_SESSION['lastname'] = $user->lastname;
                    $_SESSION['email'] = $user->email;
                    $_SESSION['profilePicture'] = $user->profilepicture;

                    header("Location: /post");
                    return true;
               } else {
                    $_SESSION['error'] = "Wrong username or password. Please try again.";
                    header("Location: /user/login");
               }
          } else {
               $_SESSION['error'] = "Wrong username or password. Please try again.";
               header("Location: /user/login");
               return false;
          }
     }

     public static function logout() {
          // TODO: Mit unset die Session-Werte löschen
          unset($_SESSION);
          // TODO: Session zerstören
          session_destroy();
          header("Location: /post");
     }

     public static function isAuthenticated() {
          // Zurückgeben ob eine ID in der Session gespeichert wurde (true/false)
          if (isset($_SESSION['userID'])) {
               return true;
          } else {
               return false;
          }
     }

     public static function getAuthenticatedUser() {
          // TODO: User anhand der ID aus der Session auslesen

          // TODO: User zurückgeben
     }

     public static function restrictAuthenticated() {
          if (!self::isAuthenticated()) {
               throw new RuntimeException("Sie haben keine Berechtigung diese Seite anzuzeigen.");
               // Unbefungte Zugriffsversuche sollten immer geloggt werden
               // z.B. mit error_log()
               exit();
          }
     }
}