<?php

namespace App\Controller;

use App\Repository\LikeRepository;
use App\Repository\PostRepository;
use App\View\View;

class PostController {
     public function index() {
          $postRepository = new PostRepository();
          $likeRepository = new LikeRepository();

          $view = new View('post/index');
          $view->title = 'Post';
          $view->heading = 'Post';
          $view->user = $_SESSION;
          if (empty($_SESSION['userID'])) {
               $_SESSION['userID'] = null;
          }
          $posts = $postRepository->readAllPosts(($_SESSION['userID']));
          foreach ($posts as $post) {
               $post->likes = $likeRepository->countByPostId($post->id);
          }
          $view->posts = $posts;
          $view->display();
     }

     public function create() {
          $view = new View('post/create');
          $view->title = 'Post';
          $view->heading = 'Post';
          $view->display();
     }

     public function doCreate() {
          date_default_timezone_set('Europe/Berlin');

          if (isset($_POST['send'])) {
               if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
                    $title = $_POST['title'];
                    $text = $_POST['text'];
                    $userID = $_SESSION['userID'];
                    $date = date('Y-m-d H:i:s');
                    $postRepository = new PostRepository();
                    $postRepository->create($title, $text, $userID, $date);
               }
          }
          header('Location: /post');
     }

     public function doEdit() {
          $json = file_get_contents("php://input");
          $params = json_decode($json);

          if (!is_numeric($params->postID)) {
               http_response_code(400);
               $this->index();
               die();
          }
          $postRepository = new PostRepository();
          $post = $postRepository->readByPostId($params->postID);
          if ($post->user_id == $_SESSION['userID']) {
               $title = $params->title;
               $text = $params->text;
               $postID = $params->postID;
               $postRepository = new PostRepository();
               $postRepository->update($title, $text, $postID);
          }

          header('Location: ' . $params->URI);
     }

     public function delete() {

          $json = file_get_contents("php://input");
          $params = json_decode($json);

          $postRepository = new PostRepository();
          $post = $postRepository->readById($params->postID);
          if ($post->user_id == $_SESSION['userID']) {
               $postRepository->deleteById($params->postID);
          }
          header('Location: /post');
     }

     public function like() {
          $likeRepository = new LikeRepository();

          $json = file_get_contents("php://input");
          $params = json_decode($json);

          $userID = $_SESSION['userID'];
          $postID = $params->postID;
          if (is_numeric($postID)) {
               if ($likeRepository->readByIds($userID, $postID) == 0) {
                    $likeRepository->create($userID, $postID);
               } else {
                    $likeRepository->deleteByIds($userID, $postID);
               }
          }

     }
}