<?php

namespace App\Controller;

use App\Authentication\Authentication;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use App\View\View;

/**
 * Siehe Dokumentation im DefaultController.
 */
class UserController {
     public function index() {
          $userRepository = new UserRepository();

          $view = new View('user/index');
          $view->title = 'User';
          $view->heading = 'User';
          $view->users = $userRepository->readAll();
          $view->profilePicture = $userRepository->getProfilePicture($_SESSION['username']);
          $view->user = $_SESSION;
          $view->display();
     }

     public function getProfilePicture() {
          $userRepository = new UserRepository();
          return $userRepository->getProfilePicture($_SESSION['username']);
     }

     public function create() {
          $view = new View('user/create');
          $view->title = 'Create user';
          $view->heading = 'Create user';
          $view->user = $_SESSION;
          if (isset($_SESSION['error'])) {
               $view->errorForUser = $_SESSION["error"];
               unset($_SESSION['error']);
          }
          $view->display();
     }

     public function doCreate() {
          if (isset($_POST['send']) && isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['username']) && isset($_POST['email'])) {
               $profilePicture = '/images/profilePictures/default.png';
               $name = $_POST['firstname'];
               $lastname = $_POST['lastname'];
               $username = $_POST['username'];
               $email = $_POST['email'];
               if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    if ($_POST['password'] == $_POST['confirmPassword']) {
                         if (preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/', $_POST['password'])) {
                              $password = $_POST['password'];
                              $userRepository = new UserRepository();
                              if ($userRepository->getUserByUname($username)->num_rows == 0) {
                                   $userRepository->create($profilePicture, $name, $lastname, $username, $email, $password);
                                   $this->doLogin();
                                   return;
                              } else {
                                   $_SESSION['error'] = "Username already exists";
                              }
                         } else {
                              $_SESSION['error'] = "Password must contain 1 uppercase letter, 1 lowercase case ,1 number and one special character.";
                         }
                    } else {
                         $_SESSION['error'] = "Passwords don't match";
                    }
               } else {
                    $_SESSION['error'] = "Please enter a valid email.";
               }
          } else {
               $_SESSION['error'] = "Something went wrong! Please try again.";
          }
          header('Location: /user/create');
     }

     public function login() {
          $view = new View('user/login');
          $view->title = 'Login';
          $view->heading = 'Login';
          $view->user = $_SESSION;
          if (isset($_SESSION['error'])) {
               $view->errorForUser = $_SESSION["error"];
               unset($_SESSION['error']);
          }
          $view->display();
     }

     public function doLogin() {
          Authentication::login($_POST['username'], $_POST['password']);
     }

     public function logout() {
          Authentication::logout();
     }

     public function profile() {
          $postRepository = new PostRepository();

          $view = new View('user/profile');
          $view->title = 'Profile';
          $view->heading = 'Profile';
          $view->posts = $postRepository->readByUserId($_SESSION['userID']);
          $view->user = $_SESSION;
          if (isset($_SESSION['error'])) {
               $view->errorForUser = $_SESSION["error"];
               unset($_SESSION['error']);
          }
          if (isset($_SESSION['message'])) {
               $view->messageForUser = $_SESSION["message"];
               unset($_SESSION['message']);
          }
          $view->display();
     }

     public function editProfile() {
          if (isset($_POST['send'])) {
               $userRepository = new UserRepository();
               $newUsername = $_POST['username'];
               $newFirstname = $_POST['firstname'];
               $newLastname = $_POST['lastname'];
               $username = $_SESSION['username'];
               if (!$_FILES['profilePicture']['name'] == '') {
                    $profilePicture = '/images/profilePictures/' . basename($_FILES['profilePicture']['name']);
                    $uploadFile = "." . $profilePicture;
                    move_uploaded_file($_FILES['profilePicture']['tmp_name'], $uploadFile);
               } else {
                    $profilePicture = $userRepository->getProfilePicture($username);
               }
               if ($newUsername === $_SESSION['username']) {
                    $userRepository->editProfile($newUsername, $newFirstname, $newLastname, $profilePicture, $username);
                    $this->refresh($newUsername);
                    $_SESSION['message'] = "Successfully saved changes!";
               }
               else if ($userRepository->getUserByUname($newUsername)->num_rows == 0) {
                    $userRepository->editProfile($newUsername, $newFirstname, $newLastname, $profilePicture, $username);
                    $this->refresh($newUsername);
                    $_SESSION['message'] = "Successfully saved changes!";
               } else {
                    $_SESSION['error'] = "Username already exists";
               }
               header("Location: /user/profile");
          }
     }

     public function changePW() {
          $userRepository = new UserRepository();
          $oldPassword = $_POST['oldPassword'];
          $username = $_POST['username'];
          if ($userRepository->checkPassword($username, $oldPassword)) {
               if ($_POST['password'] == $_POST['confirmPassword']) {
                    $password = $_POST['password'];
                    if (preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/', $password)) {
                         $userRepository->changePW($password, $username);
                         $_SESSION['message'] = "Password successfully changed";
                         header("Location: /user/profile");
                    } else {
                         $_SESSION['error'] = "Password must contain 1 uppercase letter, 1 lowercase case ,1 number and one special character.";
                         header("Location: /user/profile");
                    }
               } else {
                    $_SESSION['error'] = "Passwords don't match. Please try again.";
                    header("Location: /user/profile");
               }
          } else {
               $_SESSION['error'] = "Wrong Password. Please try again.";
               header("Location: /user/profile");
          }
     }

     public function refresh($username) {
          $userRepository = new UserRepository();
          $result = $userRepository->getUserByUname($username);

          // Falls nur ein user mit den eingegebenen Daten existiert
          if ($result->num_rows == 1) {
               $user = $result->fetch_object();
               $result->close();
               $_SESSION['userID'] = $user->id;
               $_SESSION['loggedin'] = true;
               $_SESSION['username'] = $user->username;
               $_SESSION['name'] = $user->name;
               $_SESSION['lastname'] = $user->lastname;
               $_SESSION['email'] = $user->email;
               $_SESSION['profilePicture'] = $user->profilepicture;
          }
     }
}
