<?php


namespace App\Controller;

use App\Database\ConnectionHandler;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use App\View\View;

class CommentController {
     public function index() {
          $commentRepository = new CommentRepository();

          $view = new View('post/index');
          $view->title = 'Comment';
          $view->heading = 'Comment';
          $view->comments = $commentRepository->readAll();
          $view->display();
     }

     public function getComments($postID) {
          $commentRepository = new CommentRepository();
          return $commentRepository->readByPostId($postID);
     }

     public function create() {
          $view = new View('user/create');
          $view->title = 'Create user';
          $view->heading = 'Create user';
          $view->display();
     }

     public function doCreate() {
          date_default_timezone_set('Europe/Berlin');

          if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
               $json = file_get_contents("php://input");
               $params = json_decode($json);

               $text = $params->text;
               $userID = $_SESSION['userID'];
               $postID = $params->postID;
               $date = date('Y-m-d H:i:s');
               $username = $_SESSION['username'];

               $commentRepository = new CommentRepository();
               $commentRepository->create($text, $userID, $postID, $username, $date);
          }

          // Anfrage an die URI / weiterleiten (HTTP 302)
          header('Location: /post');
     }

     public function doEdit() {

          $json = file_get_contents("php://input");
          $params = json_decode($json);

          if (!is_numeric($params->commentID)) {
               http_response_code(400);
               $this->index();
               die();
          }

          $commentRepository = new CommentRepository();
          $comment = $commentRepository->readById($params->commentID);
          if ($comment->user_id == $_SESSION['userID']) {
               $text = $params->text;
               $commentID = $params->commentID;
               $commentRepository = new CommentRepository();
               $commentRepository->update($commentID, $text);
          }

          header('Location: /post');
     }

     public function delete() {

          $json = file_get_contents("php://input");
          $params = json_decode($json);

          $commentRepository = new CommentRepository();
          $comment = $commentRepository->readById($params->commentID);
          if ($comment->user_id == $_SESSION['userID']) {
               $commentRepository->deleteById($params->commentID);
          }
          header('Location: /post');
     }
}