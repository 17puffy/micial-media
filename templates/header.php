<?php

use App\Controller\UserController;

?>


<!doctype html>
<html lang="en">
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
     <link rel="stylesheet" type="text/css" href="/css/style.css">

     <title>Micial Media</title>
</head>
<body>

<header>
     <nav class="navbar navbar-expand-lg navbar-light bg-primary">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                  aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
          </button>
          <a href="/post">
               <img src="/images/logo.svg" alt="Micial Media Logo">
          </a>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
               <a class="navbar-brand" href="/post"></a>
               <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                         <a class="nav-link" href="/post">Home<span class="sr-only">(current)</span></a>
                    </li>

                    <?php
                    if (!(isset($user['loggedin']) && $user['loggedin'] == true)) { ?>
                         <li>
                              <a class="nav-link" href="/user/login">Login</a>
                         </li>
                         <li class="nav-item">
                              <a class="nav-link" href="/user/create">Sign Up</a>
                         </li>
                    <?php } else { ?>
                         <li>
                              <a class="nav-link" href="/user/profile">Profile</a>
                         </li>
                         <li>
                              <a class="nav-link" href="/user/logout">Log out</a>
                         </li>
                    <?php } ?>
               </ul>
               <!--                <button type="button" id="lightmode">-->
               <!--                    <img src="/images/sun.png" alt="lightmode">-->
               <!--                </button>-->
               <?php $userController = new UserController();
               if ((isset($user['loggedin']) && $user['loggedin'] == true)) : ?>
                    <a class="navbar-brand" href="/user/profile"><img
                                 src="<?php echo $userController->getProfilePicture() ?>"
                                 class="ProfilePicture"><?php echo htmlentities($user['username']) ?></a>
               <?php endif; ?>
               <!--                <form class="form-inline my-2 my-lg-0">-->
               <!--                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">-->
               <!--                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
               <!--                </form>-->
          </div>
     </nav>
</header>
<main class="container">
     <!--      <h1><? //= $heading; ?></h1>-->
