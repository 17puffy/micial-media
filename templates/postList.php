<!-- Listing the posts on the starting page and in the profile -->
<?php

use App\Controller\CommentController;
use App\Controller\PostController;

$postCounter = 0;
$commentCounter = 0;
if (empty($posts)): ?>
     <div>
          <h2 class="item title">There are no posts yet.</h2>
     </div>
<?php else: ?>
     <?php foreach ($posts as $post): ?>
          <?php $postCounter++; ?>
          <div class="card text-white bg-primary mb-3">
               <div class="card-header">

                    <!-- printing the username of the creator of the post. Its blue if its your own post. -->
                    <div class="grid">
                         <img src="<?php echo $post->profilepicture ?>" class="ProfilePicture" alt="Profile Picture of <?php echo $post->username ?>">
                        <div>
                            <?php if (isset($user['loggedin']) && $user['loggedin']) : ?>
                                <?php if ($post->username == $user['username']) : ?>
                                    <a style="font-size: 15pt; color: #69a8bb; font-weight: bolder; text-decoration: none;"
                                       href="/user/profile">@<?= htmlentities($post->username) ?></a>
                                <?php else: ?>
                                    <strong style="font-size: 15pt;">@<?= htmlentities($post->username) ?></strong>
                                <?php endif ?>
                            <?php else: ?>
                                <strong style="font-size: 15pt;">@<?= htmlentities($post->username) ?></strong>
                            <?php endif;
                            if ($post->verification): ?>
                            <img class="verification" src="/images/verification.png" alt="Verification image">
                            <?php endif; ?>
                        </div>
                         <small>
                              <time><?php echo htmlentities($post->date) ?></time>
                         </small>
                    </div>

                    <!-- "delete" and "edit" button. Only available if its your own post. -->
                    <?php if (isset($user['userID'])): ?>
                         <?php if ($post->username == $user['username']): ?>
                              <div class="dropdown">
                                   <a class="btn btn-secondary" href="#" role="button" id="dropdownMenuLink"
                                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="/images/dropdown.png" alt="Dropdown menu of post">
                                   </a>
                                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink"
                                        style="left: ">
                                        <button type="button" class="dropdown-item" data-toggle="modal"
                                                data-target="#postModal<?php echo $postCounter ?>">Delete
                                        </button>
                                        <a class="dropdown-item" type="button" data-toggle="collapse"
                                           href=".PostEdit<?php echo $postCounter ?>" role="button"
                                           aria-expanded="false"
                                           aria-controls="multiCollapseExample1">Edit</a>
                                   </div>
                              </div>
                         <?php endif; ?>
                    <?php endif; ?>

                    <!-- Modal to confirm deletes -->
                    <div class="modal fade" id="postModal<?php echo $postCounter ?>" data-backdrop="static"
                         data-keyboard="false" tabindex="-1"
                         aria-labelledby="staticBackdropLabel<?php echo $postCounter ?>" aria-hidden="true">
                         <div class="modal-dialog modal-dialog-centered">
                              <div class="modal-content">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel<?php echo $postCounter ?>">Delete</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>
                                   <div class="modal-body">
                                        Are you sure you want to delete this post?
                                   </div>
                                   <div class="modal-footer">
                                        <form onsubmit="return false" class="postDelete-btn">
                                             <input type="number" hidden name="postID" value="<?php echo $post->id ?>">
                                             <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel
                                             </button>
                                             <button type="submit" class="btn btn-primary">Delete</button>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>

               <!-- The post -->
               <div class="card-body">
                    <div class="collapse multi-collapse PostEdit<?php echo $postCounter ?> "
                         id="idPostEdit<?php echo $postCounter ?>">
                         <form onsubmit="return false" class="postEdit-btn">
                              <input class="postID" type="hidden" name="postID" value="<?php echo $post->id ?>">
                              <input class="URI" type="hidden" name="URI" value="<?php echo $_SERVER['REQUEST_URI'] ?>">
                              <input class="form-control form-control-lg title" type="text" name="title"
                                     value="<?php echo htmlentities($post->title) ?>" autocomplete="off" required>
                              <textarea class="form-control" name="text" placeholder="Text" required
                                        rows="4"
                                        autocomplete="off"><?php echo htmlentities($post->text) ?></textarea>
                              <button class="btn btn-primary" type="submit" name="send"><img src="/images/send.png" alt="Send button">
                              </button>
                         </form>
                    </div>
                    <div class="collapse show multi-collapse PostEdit<?php echo $postCounter ?>">
                         <h5 class="card-title"><?= htmlentities($post->title) ?></h5>
                         <p class="card-text"><?= htmlentities($post->text) ?></p>
                    </div>
               </div>

               <!-- Comment section -->
               <?php if ($_SERVER['REQUEST_URI'] != '/user/profile') : ?>
                    <div class="card-footer">
                         <div class="d-flex flex-row">
                              <?php if (isset($user['loggedin']) && $user['loggedin']) :
                                   $btnCommentData = "data-toggle=\"collapse\" href=\"#CommentSection$postCounter\"  role=\"button\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample1\"";
                                   ($post->like) ? $imgLikeData = 'Red' : $imgLikeData = ''; ?>
                                   <form onsubmit="return false" class="like-btn">
                                        <input type="number" hidden name="postID" value="<?php echo $post->id ?>">
                                        <button class="btn primary btnComment" type="submit"><img class="heart"
                                                                                                  src="/images/heart<?= $imgLikeData ?>.png" alt="<?= $imgLikeData ?> Heart">
                                        </button>
                                   </form>
                              <?php else :
                                   $btnCommentData = "href=\"/user/login\""; ?>
                                   <a class="btn primary btnComment" href="/user/login"><img class="heart"
                                                                                             src="/images/heart.png" alt="White Heart"></a>
                              <?php endif; ?>
                              <a class="btn primary btnComment" <?php echo $btnCommentData ?>>
                                   <img src="/images/bubbleWhite.png" style="width:2rem;" alt="Comment button"></a>
                              <p class="align-self-center"><?= $post->likes ?> Like<?php if($post->likes != 1) {echo "s";}?></p>
                         </div>
                         <div class="row">
                              <div class="collapse multi-collapse" id="<?php echo "CommentSection" . $postCounter ?>">

                                   <!-- listing the comments -->
                                   <?php
                                   $commentController = new CommentController();
                                   $commentsOfPost = $commentController->getComments($post->id);

                                   if (empty($commentsOfPost)): ?>
                                        <div class="card card-header">
                                             <p>There are no Comments on this post yet.</p>
                                        </div>
                                   <?php else: ?>
                                        <?php while ($comment = $commentsOfPost->fetch_object()):
                                             $commentCounter++; ?>
                                             <div class="card-body mb-2">
                                                  <div class="flex">
                                                       <div class="grid">
                                                            <img src="<?php echo $comment->profilepicture ?>"
                                                                 class="ProfilePicture" alt="Profile picture of <?php echo $comment->username ?>">
                                                            <?php if (isset($user['userID'])) : ?>
                                                                 <?php if ($comment->username == $user['username']) : ?>
                                                                      <a style="font-size: 15pt; color: #69a8bb; font-weight: bolder; text-decoration: none;"
                                                                         href="/user/profile">@<?= htmlentities($comment->username) ?></a>
                                                                 <?php else: ?>
                                                                      <strong style="font-size: 15pt;">@<?= htmlentities($comment->username) ?></strong>
                                                                 <?php endif ?>
                                                            <?php else: ?>
                                                                 <strong style="font-size: 15pt;">@<?= htmlentities($comment->username) ?></strong>
                                                            <?php endif ?>
                                                            <small>
                                                                 <time><?php echo htmlentities($comment->date) ?></time>
                                                            </small>
                                                       </div>
                                                       <?php if (isset($user['userID'])): ?>
                                                            <?php if ($comment->username == $user['username']): ?>
                                                                 <div class="dropdown">
                                                                      <a class="btn btn-secondary" href="#"
                                                                         role="button" id="dropdownMenuLink"
                                                                         data-toggle="dropdown" aria-haspopup="true"
                                                                         aria-expanded="false">
                                                                           <img src="/images/dropdown.png" alt="Dropdown menu of comment">
                                                                      </a>
                                                                      <div class="dropdown-menu dropdown-menu-right"
                                                                           aria-labelledby="dropdownMenuLink"
                                                                           style="left: ">
                                                                           <button type="button" class="dropdown-item"
                                                                                   data-toggle="modal"
                                                                                   data-target="#commentModal<?php echo $commentCounter ?>">
                                                                                Delete
                                                                           </button>
                                                                           <a class="dropdown-item" type="button"
                                                                              data-toggle="collapse"
                                                                              href=".CommentEdit<?php echo $commentCounter ?>"
                                                                              role="button"
                                                                              aria-expanded="false"
                                                                              aria-controls="multiCollapseExample1">Edit</a>
                                                                      </div>
                                                                 </div>
                                                            <?php endif; ?>
                                                       <?php endif; ?>

                                                  </div>
                                                  <div class="collapse multi-collapse CommentEdit<?php echo $commentCounter ?> "
                                                       id="idCommentEdit<?php echo $commentCounter ?>">
                                                       <form onsubmit="return false" class="commentEdit-btn">
                                                            <input type="hidden" name="commentID"
                                                                   value="<?php echo $comment->id ?>">
                                                            <textarea class="form-control" name="text"
                                                                      placeholder="Text" required
                                                                      rows="2"
                                                                      autocomplete="off"><?php echo htmlentities($comment->text) ?></textarea>
                                                            <button class="btn btn-primary" type="submit" name="send">
                                                                 <img src="/images/send.png" alt="Send button">
                                                            </button>
                                                       </form>
                                                  </div>
                                                  <div class="collapse show multi-collapse CommentEdit<?php echo $commentCounter ?>">
                                                       <p class="card-text"><?= htmlentities($comment->text) ?></p>
                                                  </div>

                                                  <!-- Modal to confirm deletes -->
                                                  <div class="modal fade" id="commentModal<?php echo $commentCounter ?>"
                                                       data-backdrop="static"
                                                       data-keyboard="false" tabindex="-1"
                                                       aria-labelledby="staticBackdropLabelComment<?php echo $commentCounter ?>" aria-hidden="true">
                                                       <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                 <div class="modal-header">
                                                                      <h5 class="modal-title" id="staticBackdropLabelComment<?php echo $commentCounter ?>">
                                                                           Delete</h5>
                                                                      <button type="button" class="close"
                                                                              data-dismiss="modal" aria-label="Close">
                                                                           <span aria-hidden="true">&times;</span>
                                                                      </button>
                                                                 </div>
                                                                 <div class="modal-body">
                                                                      Are you sure you want to delete this comment?
                                                                 </div>
                                                                 <div class="modal-footer">
                                                                      <form onsubmit="return false" class="commentDelete-btn">
                                                                           <input type="number" hidden name="commentID"
                                                                                  value="<?php echo $comment->id ?>">
                                                                           <button type="button" class="btn btn-primary"
                                                                                   data-dismiss="modal">Cancel
                                                                           </button>
                                                                           <button type="submit"
                                                                                   class="btn btn-primary">Delete
                                                                           </button>
                                                                      </form>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                        <?php endwhile; ?>
                                   <?php endif ?>

                                   <form onsubmit="return false" autocomplete="off" class="comment-btn">
                                        <textarea class="form-control" name="text" placeholder="Add comment"
                                                  required></textarea>
                                        <input class="form-control" type="number" name="postID"
                                               value="<?php echo htmlentities($post->id); ?>" hidden>
                                        <button class="btn-primary btn" type="submit" name="send"><img
                                                     src="/images/send.png"
                                                     style="width: 30px" alt="Send button">
                                        </button>
                                   </form>
                              </div>
                         </div>
                    </div>
               <?php endif; ?>
          </div>
     <?php endforeach; ?>
<?php endif; ?>