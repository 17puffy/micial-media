<?php
if (isset($user['loggedin']) && $user['loggedin']) {
     $btnPostData = "class=\"btn btn-primary btn-color-inverted\" data-toggle=\"collapse\" href=\"#PostInput\" role=\"button\" aria-expanded=\"false\" aria-controls=\"multiCollapseExample1\"";
} else {
     $btnPostData = "class=\"btn btn-primary\" href=\"/user/login\"";
} ?>

     <a role="button" <?= $btnPostData ?>>
          <img src="/images/plus.png" alt="Create post">New post</a>

     <div class="collapse multi-collapse w-100 m-0 mb-3" id="PostInput">
          <div class="card card-body">
               <form action="/post/doCreate" method="post" autocomplete="off">
                    <input class="form-control form-control-lg" type="text" name="title" placeholder="Title" required>
                    <textarea class="form-control" name="text" placeholder="Text" required
                              rows="4"></textarea>
                    <button class="btn btn-primary" type="submit" name="send"><img src="/images/send.png"
                                                                                   alt="send">
                    </button>
               </form>
          </div>
     </div>
<?php require_once "./../templates/postList.php" ?>