<?php if (isset($user['loggedin']) && !$user['loggedin']) {
     header("Location: /post");
} ?>

<h1>Login</h1>
<div class="row">
     <form action="/user/doLogin" method="post" class="col-6">
          <div class="form-group">
               <input id="username" name="username" type="text" placeholder="Username" class="form-control" required
                      autofocus autocomplete="off">
          </div>
          <div class="form-group">
               <input id="password" name="password" type="password" placeholder="Password" class="form-control" required
                      autocomplete="off">
          </div>
          <?php if (isset($errorForUser)): ?>
               <div class="alert alert-danger" role="alert">
                    <?php echo $errorForUser ?>
               </div>
          <?php endif;
          $errorForUser = null; ?>

          <button type="submit" name="send" class="btn btn-primary">Log in</button>
     </form>
</div>
<p>Don't have an account yet? <a href="/user/create">Sign up</a></p>
