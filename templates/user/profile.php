<?php

use App\Controller\UserController;

if (isset($user) && !$user['loggedin']) {
     header("Location: /post");
} ?>

<div class="row">
     <div class="col-3">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
               <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-profile" role="tab"
                  aria-controls="v-pills-profile" aria-selected="true">Profile</a>
               <a class="nav-link" id="v-pills-edit-tab" data-toggle="pill" href="#v-pills-edit" role="tab"
                  aria-controls="v-pills-edit" aria-selected="false">Edit Profile</a>
               <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab"
                  aria-controls="v-pills-messages" aria-selected="false">Your Posts</a>
          </div>
     </div>
     <div class="col-9">
          <div class="tab-content" id="v-pills-tabContent">

               <!-- Profile -->
               <div class="tab-pane fade show active col-6 col-6-profile" id="v-pills-profile" role="tabpanel"
                    aria-labelledby="v-pills-profile-tab">
                    <img src=" <?php $userController = new UserController();
                    echo $userController->getProfilePicture() ?>" class="ProfilePictureBig" alt="Profile picture">
                    <p id="uname"><?php echo htmlentities($user['username']) ?></p>
                    <p id="name"><?php echo htmlentities($user['name']) . " " . htmlentities($user['lastname']) ?></p>
                    <p id="email"><?php echo htmlentities($user['email']) ?></p><br>
                    <?php if (isset($errorForUser)): ?>
                         <div class="alert alert-danger" role="alert">
                              <?php echo $errorForUser ?>
                         </div>
                    <?php endif;
                    if (isset($messageForUser)): ?>
                         <div class="alert alert-message" role="alert">
                              <?php echo $messageForUser ?>
                         </div>
                    <?php endif;
                    $errorForUser = null;
                    $messageForUser = null; ?>
               </div>

               <!-- Edit Profile -->
               <div class="tab-pane fade" id="v-pills-edit" role="tabpanel" aria-labelledby="v-pills-edit-tab">
                    <form action="/user/editProfile" method="post" class="col-6 col-6-profile"
                          enctype="multipart/form-data">
                         <!-- <label for="profilePicture">Profile picture</label> -->
                         <div class="file-input">
                              <img src=" <?php $userController = new UserController();
                              echo $userController->getProfilePicture() ?>" class="ProfilePictureBig"
                                   alt="Profile picture">
                              <input type="file" id="file" name="profilePicture"
                                     class="file form-control profilePicture">
                              <label for="file"><img src="/images/pen.png" class="pen">
                              </label>
                         </div>
                         <label for="username">Username</label>
                         <input id="username" name="username" type="text" class="form-control"
                                placeholder="New Username"
                                value="<?php echo htmlentities($user['username']) ?>" autocomplete="off">
                         <label for="firstname">First name</label>
                         <input id="firstname" name="firstname" type="text" class="form-control"
                                placeholder="New first name"
                                value="<?php echo htmlentities($user['name']) ?>" autocomplete="off">
                         <label for="lastname">Last name</label>
                         <input id="lastname" name="lastname" type="text" class="form-control"
                                placeholder="New last name"
                                value="<?php echo htmlentities($user['lastname']) ?>" autocomplete="off">

                         <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                              Change password
                         </button>
                         <br>
                         <button type="submit" name="send" class="btn btn-primary">Save changes</button>
                    </form>
               </div>

               <!-- Your Posts -->
               <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                    <?php require_once "./../templates/postList.php"; ?>
               </div>

               <!-- Modal -->
               <div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                         <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                   </button>
                              </div>
                              <div class="modal-body">
                                   <form action="/user/changePW" method="post">
                                        <input id="oldPassword" name="oldPassword" type="password" class="form-control"
                                               placeholder="Old password" required autofocus>
                                        <input id="password" name="password" type="password" class="form-control"
                                               placeholder="New password" required>
                                        <input id="confirmPassword" name="confirmPassword" type="password"
                                               class="form-control"
                                               placeholder="Confirm password" required>
                                        <input name="username" hidden
                                               value="<?php echo htmlentities($user['username']); ?> ">
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                   </form>
                                   <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                              </div>
                         </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                         aria-labelledby="v-pills-messages-tab">
                         <?php require_once "./../templates/postList.php"; ?>
                    </div>
               </div>
          </div>
     </div>


     <!-- Your Posts -->
