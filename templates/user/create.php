<?php if (isset($user['loggedin']) && $user['loggedin']) {
     header("Location: /post");
} ?>

<h1>Sign up</h1>
<div class="row">
     <form action="/user/doCreate" method="post" class="col-6" enctype="multipart/form-data">
          <div class="form-group">
               <input id="firstname" name="firstname" type="text" placeholder="Name" class="form-control"
                      autocomplete="off" required autofocus>
          </div>

          <div class="form-group">
               <input id="lastname" name="lastname" type="text" placeholder="Last name" class="form-control" required
                      autocomplete="off">
          </div>

          <div class="form-group">
               <input id="username" name="username" type="text" placeholder="Username" class="form-control" required
                      autocomplete="off">
          </div>

          <div class="form-group">
               <input id="email" name="email" type="email" placeholder="Mail" class="form-control" required
                      autocomplete="off">
          </div>

          <div class="form-group">
               <input id="password" name="password" type="password" placeholder="Password" class="form-control" required
                      autocomplete="off">
          </div>

          <div class="form-group">
               <input id="password" name="confirmPassword" type="password" placeholder="Confirm password"
                      class="form-control" required autocomplete="off">
          </div>
          <?php if (isset($errorForUser)): ?>
               <div class="alert alert-danger" role="alert">
                    <?php echo $errorForUser ?>
               </div>
          <?php endif;
          $errorForUser = null; ?>
          <button type="submit" name="send" class="btn btn-primary">Create user</button>
     </form>
</div>
<p>Already signed up? <a href="/user/login">Login</a></p>