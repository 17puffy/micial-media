let likeElements = document.querySelectorAll(".like-btn")
for (let i = 0; i < likeElements.length; i++) {
    likeElements[i].addEventListener("submit", function () {
        fetch('/post/like', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"postID": likeElements[i].querySelector("input").value})
        }).then(function () {
            location.reload();
        })
    })
}

let commentElements = document.querySelectorAll(".comment-btn")
for (let i = 0; i < commentElements.length; i++) {
    commentElements[i].addEventListener("submit", function () {
        fetch('/comment/doCreate', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"postID": commentElements[i].querySelector("input").value, "text": commentElements[i].querySelector("textarea").value})
        }).then(function () {
            location.reload();
        })
    })
}

let commentEditElements = document.querySelectorAll(".commentEdit-btn")
for (let i = 0; i < commentEditElements.length; i++) {
    commentEditElements[i].addEventListener("submit", function () {
        fetch('/comment/doEdit', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"commentID": commentEditElements[i].querySelector("input").value, "text": commentEditElements[i].querySelector("textarea").value})
        }).then(function () {
            location.reload();
        })
    })
}

let postEditElements = document.querySelectorAll(".postEdit-btn")
for (let i = 0; i < postEditElements.length; i++) {
    postEditElements[i].addEventListener("submit", function () {
        fetch('/post/doEdit', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"postID": postEditElements[i].querySelector(".postID").value,
                "URI": postEditElements[i].querySelector(".URI").value,
                "title": postEditElements[i].querySelector(".title").value,
                "text": postEditElements[i].querySelector("textarea").value})
        }).then(function () {
            location.reload();
        })
    })
}

let postDeleteElements = document.querySelectorAll(".postDelete-btn")
for (let i = 0; i < postDeleteElements.length; i++) {
    postDeleteElements[i].addEventListener("submit", function () {
        fetch('/post/delete', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"postID": postDeleteElements[i].querySelector("input").value})
        }).then(function () {
            location.reload();
        })
    })
}

let commentDeleteElements = document.querySelectorAll(".commentDelete-btn")
for (let i = 0; i < commentDeleteElements.length; i++) {
    commentDeleteElements[i].addEventListener("submit", function () {
        fetch('/comment/delete', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"commentID": commentDeleteElements[i].querySelector("input").value})
        }).then(function () {
            location.reload();
        })
    })
}